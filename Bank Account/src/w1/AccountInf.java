package w1;

public class AccountInf {
	public static void main(String[] args) {
		Account account1 = new Account();
		account1.number= 1;
		account1.balance = 100;
		account1.currency = "TL";
		
		Account account2 = new Account();
		account2.number= 2;
		account2.balance = 200;
		account2.currency = "USD";
		
		account1.report();
		account2.report();
		
		account1.deposit(50);
		account2.deposit(300);
		
		account1.report();
		account2.report();
	}

	ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDvpjlSjnhsumj5u9HC3DCVF/Ok3Suc4Q5IJI4Mnn7y/A6YdH3MLbC7v1NvAca5RD0yVxDKhWGCU77dd/DZM1u1/Rouch1ILIzCfeGJ8JaSXbnFKBunktqosgBmVMzc2qxjgLUG9zz/PXmWLhJZWPwMpnDmlM5xcuU6V7RiDtKMKUM1ptgSan80waQrp64MmFfl4pWY/gPm1ON/kEZQcGicUVX7eA59T+MMM4cc9co2sYse0+oJvIWWXfUx3DcQN9wvBspaeH0RjYTGBtvvySAx/T3otGWGAxhlS7tGzigcEIMWnfVdmJc3sm3gQuPrquWLo3ZkNrOoYJk7cpX8Van5 elif.ozmen@ozu.edu.tr
	

}
